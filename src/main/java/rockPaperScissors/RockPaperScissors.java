package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import java.util.Random;

public class RockPaperScissors {
    
    public static void main(String[] args) {
        /*  
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    List<String> continueChoices = Arrays.asList("y", "n");
    

    public void run() {
        while (true){
            System.out.println("Let's play round "+roundCounter);
            roundCounter+=1;
            String humanChoice;
            while (true){
                humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
                if (!rpsChoices.contains(humanChoice)) {
                    System.out.println("I do not understand " + humanChoice + " Could you try again?");
                }else{
                    break;
                }
            }

            Random rand = new Random();
            String randomElement = rpsChoices.get(rand.nextInt(rpsChoices.size()));

            String whoWon = ". Human wins!";
            if (humanChoice.equals("rock")&& randomElement.equals("scissors")) {
                humanScore+=1;
            }
            else if (humanChoice.equals("paper")&& randomElement.equals("rock")) {
                humanScore+=1;
            }
            else if (humanChoice.equals("scissors")&& randomElement.equals("paper")){
                humanScore+=1;
            }
            else if (humanChoice.equals(randomElement)){
                whoWon=(". It's a tie!");
            }
            else{
                computerScore+=1;
                whoWon=(". Computer wins!)");
            }
            
            System.out.println("Human chose " + humanChoice + " computer chose " + randomElement + whoWon);
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            
            String continuePlay;
            while (true){
                continuePlay = readInput("Do you wish to continue playing? (y/n)? ");
                if (!continueChoices.contains(continuePlay)){
                    System.out.println("I do not understand " + continuePlay + ". Try again.");}
                else{
                    break;
                }
                
            }
            if (continuePlay.equals("n")){
                break;

            }
        }
        System.out.println("Bye bye :)");
        
    }


    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}

